local event = require "event"
local shell = require "shell"
local thread = require "thread"

local threads = {}

-- # Service methods (notice they aren't local variables. That makes them service methods)

function start(prog, ...)
  -- # start a program as a thread process.
  local proc = thread.create(shell.execute, prog, os.getenv("shell"), ...)
  -- # detach it from current process so rc doesn't block the main thread when we start.
  proc:detach()
  -- # keep track of started processes
  table.insert(threads, proc)
end

function stop()
  -- # kill all not 'dead' threads service started
  for i, proc in ipairs(threads) do
    if proc.status and proc:status() ~= "dead" then
      proc:kill() -- # stabby stab...
    end
  end
end