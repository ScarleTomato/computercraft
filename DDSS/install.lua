
local internet = require('internet')

repo = 'https://gitlab.com/ScarleTomato/computercraft/-/raw/main/DDSS'
regfile = '/home/ddss.reg'

function reg(str)
  local file = io.open(regfile, "a")
  file:write(str .. string.char(10))
  io.close(file)
end

function dl(path)
  -- download each file and save the path to the commit file
  print("downloading " .. path)
  os.execute("wget " .. repo .. path .. " " .. path)
  reg('file ' .. path)
end

function md(path)
  -- make a directory save the path to the commit file
  os.execute("mkdir " .. path)
  reg('dir ' .. path)
end

function getLatestCommit()
  local handle = internet.request('https://gitlab.com/api/v4/projects/44239034/repository/branches?search=main')
  local result = ''
  for chunk in handle do result = result .. chunk end
  return string.sub(result,33,72)
end

function uninstall()

  os.execute("rc mainservice stop ddss/autoc")

  -- for each file in regfile remove it and then delete the regfile
  local file = io.open(regfile)
  if file ~= nil then
    local lines = file:lines()
    for line in lines do
      if string.sub(line,1,4) == 'file' then
        print('removing file ' .. string.sub(line,6))
        os.execute("rm " .. string.sub(line,6))
      end
    end
    io.close(file)
  end
end

function install()
  os.execute('rm ' .. regfile)
  local commit = getLatestCommit()
  reg('commit ' .. commit)
  os.execute("wget " .. repo .. '/etc/rc.d/mainservice.lua' .. " " .. '/etc/rc.d/mainservice.lua')
  md('/home/ddss')
  dl('/home/ddss/updater.lua')
  dl('/home/ddss/autoc.lua')

  os.execute("rc mainservice start ddss/autoc")
  -- os.execute("rc mainservice start ddss/updater")
end

function checkInstall()
  local f = io.open(regfile)
  if f == nil then
    return true
  end
  local localCommit = string.sub(f:read(),8)
  io.close(f)
  return localCommit ~= getLatestCommit()
end

if checkInstall() then
  uninstall()
  install()
end
