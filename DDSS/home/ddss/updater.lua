
internet = require('internet')

loopDelay = 60 -- Seconds between runs

function getLatestCommit()
  local handle = internet.request('https://gitlab.com/api/v4/projects/44239034/repository/branches?search=main')
  local result = ''
  for chunk in handle do result = result .. chunk end
  return string.sub(result,33,72)
end

function checkInstall()
  local f = io.open(regfile)
  if f == nil then
    return true
  end
  local localCommit = string.sub(f:read(),8)
  io.close(f)
  return localCommit ~= getLatestCommit()
end

while true do
  if checkInstall() then
    os.execute('rm install.lua ; wget https://gitlab.com/ScarleTomato/computercraft/-/raw/main/DDSS/install.lua && ./install')
  end
  os.sleep(loopDelay)
end