-- Original by Palagius : https://oc.cil.li/index.php?/topic/1426-ae2-level-auto-crafting/
-- Modfied by Dalden 2018-07-28 
--           - Store crafting result object to check for status
--           - If crafting job is not yet finished from previous cycle then skip this cycle

local component = require("component")
local sides = require("sides")
local meController = component.proxy(component.me_controller.address)
local inv = component.inventory_controller
local gpu = component.gpu

-- Each element of the array is "item", "damage", "number wanted", "max craft size"
-- Damage value should be zero for base items

requests = {}
items = {
    { "minecraft:glass",       0, 100, 100 },
    { "minecraft:planks",       0, 100, 100 },
    { "minecraft:iron_ingot",       0, 100, 100 },
    { "minecraft:gold_ingot",       0, 100, 100 },
    { "minecraft:glass",      0, 100, 100 },
    { "minecraft:quartz",      0, 100, 100 },
    { "minecraft:diamond",      0, 100, 100 },
    { "minecraft:emerald",      0, 100, 100 },
    { "draconicevolution:draconium_ingot",      0, 100, 100 },
    { "thermalfoundation:material",       128, 100, 100 }, -- Copper Ingot
    { "thermalfoundation:material",       129, 100, 100 }, -- Tin Ingot
    { "thermalfoundation:material",       130, 100, 100 }, -- Silver Ingot
    { "thermalfoundation:material",       131, 100, 100 }, -- Lead Ingot
    { "thermalfoundation:material",       161, 100, 100 }, -- Electrum Ingot
    { "thermalfoundation:material",       162, 100, 100 }, -- Invar Ingot
    { "thermalfoundation:material",       163, 100, 100 }, -- Bronze Ingot
    { "thermalfoundation:material",       164, 100, 100 }, -- Constantan Ingot
    { "thermalfoundation:material",       165, 100, 100 }, -- Signalum Ingot
    { "thermalfoundation:material",       166, 100, 100 }, -- Lumium Ingot
    { "thermalfoundation:material",       167, 100, 100 }, -- Enderium Ingot
    { "appliedenergistics2:material",       24, 100, 100 }, -- Engineering Processor
    { "appliedenergistics2:material",       23, 100, 100 }, -- Calculation Processor
    { "appliedenergistics2:material",       22, 100, 100 }, -- Logic Processor
--    { "appliedenergistics2:material",       11, 100, 100 }, -- Pure Nether Quartz Crystal
    { "appliedenergistics2:material",       10, 100, 100 }, -- Pure Certus Quartz Crystal
    { "appliedenergistics2:material",       7, 100, 100 }, -- Fluix Crystal
--    { "appliedenergistics2:material",       12, 100, 100 }, -- Pure Fluix Crystal
    { "appliedenergistics2:material",       0, 100, 100 }, -- Certus Quartz Crystal
    { "appliedenergistics2:material",       1, 100, 100 }, -- Charged Certus Quartz Crystal
    { "appliedenergistics2:material",       8, 100, 100 }, -- Fluix Dust
--    { "appliedenergistics2:material",       2, 100, 100 }, -- Certus Quartz Dust
    { "nuclearcraft:gem_dust",       2, 100, 100 }, -- Crushed Quartz
}

function readItemsFromChest()
    local items = {}
    for s=1,inv.getInventorySize(sides.posz) do
        stack = inv.getStackInSlot(sides.posz, s)
        if stack ~= nil then
            table.insert(items, {stack.name, stack.damage, stack.size * 10, stack.size * 10})
        end
    end
    return items
end

loopDelay = 10 -- Seconds between runs

-- Init list with crafting status
-- for curIdx = 1, #items do
--     requests[items[curIdx][1]][1] = false -- Crafting status set to false
--     requests[items[curIdx][1]][2] = nil -- Crafting object null
-- end

while true do
    items = readItemsFromChest()
    for curIdx = 1, #items do
        curName = items[curIdx][1]
        curDamage = items[curIdx][2]
        curMinValue = items[curIdx][3]
        curMaxRequest = items[curIdx][4]
        curCraftStatus = requests[curName]
        curCrafting = curCraftStatus ~= nil

        -- io.write("Checking for " .. curMinValue .. " of " .. curName .. "\n")
        storedItem = meController.getItemsInNetwork({
            name = curName,
            damage = curDamage
            })
        io.write("Network contains ")
        gpu.setForeground(0xCC24C0) -- Purple-ish
        io.write(storedItem[1].size)
        gpu.setForeground(0xFFFFFF) -- White
        io.write(" items with label ")
        gpu.setForeground(0x00FF00) -- Green
        io.write(storedItem[1].label .. "\n")
        gpu.setForeground(0xFFFFFF) -- White
        if storedItem[1].size < curMinValue then
            delta = curMinValue - storedItem[1].size
            craftAmount = delta
            if delta > curMaxRequest then
                craftAmount = curMaxRequest
            end

            io.write("  Need to craft ")
            gpu.setForeground(0xFF0000) -- Red
            io.write(delta)
            gpu.setForeground(0xFFFFFF) -- White
            io.write(", requesting ")
            gpu.setForeground(0xCC24C0) -- Purple-ish
            io.write(craftAmount .. "... ")
            gpu.setForeground(0xFFFFFF) -- White

            craftables = meController.getCraftables({
                name = curName,
                damage = curDamage
                })
            if craftables.n >= 1 then
                cItem = craftables[1]
                if curCrafting then
                    if curCraftStatus.isCanceled() or curCraftStatus.isDone() then
                        io.write("Previous Craft completed\n")
                        requests[curName] = nil
                        curCrafting = false
                    end
                end
                if curCrafting then
                        io.write("Previous Craft busy\n")
                end
                if not curCrafting then
                    retval = cItem.request(craftAmount)
                    curCrafting = true
                    requests[curName] = retval
                    gpu.setForeground(0x00FF00) -- Green
                    io.write("Requested - ")
		    --while (not retval.isCanceled()) and (not retval.isDone()) do
	            --		os.sleep(1)
                    --        io.write(".")
		    -- end
                    gpu.setForeground(0xFFFFFF) -- White
                    io.write("Done \n")
                end
            else
                gpu.setForeground(0xFF0000) -- Red
                io.write("    Unable to locate craftable for " .. storedItem[1].name .. "\n")
                gpu.setForeground(0xFFFFFF) -- White
            end
        end
    end
    io.write("Sleeping for " .. loopDelay .. " seconds...\n\n")
    os.sleep(loopDelay)
end